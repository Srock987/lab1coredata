//
//  ReadingManager.swift
//  Lab1CoreData
//
//  Created by Pawel Srokowski on 15/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation
import CoreData

class ReadingManager {
    static func fetchReadings() -> [Reading] {
        let fetchRequest:  NSFetchRequest<Reading> = Reading.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Reading.timestamp), ascending: true)
        fetchRequest.sortDescriptors = [sort]

        var fetchedReading: [Reading] = []
        do{
           fetchedReading  = try PersistenService.context.fetch(fetchRequest)
        } catch {}
        return fetchedReading
    }
    
    static func createReadings(number: Int){
        let sensors: [Sensor] = SensorManager.fetchSensors()
        for index in 1...number{
            let newReading = NSEntityDescription.insertNewObject(forEntityName: "Reading", into: PersistenService.context) as! Reading
            newReading.value = randomFloat()
            newReading.timestamp = randomDateFromYearBefore()
            let sensor = sensors[randomInt(max: sensors.count)]
            newReading.sensor = sensor
            //sensor.readings?.adding(newReading)
            if(index % 1000 == 0){
                print(String(format: "%d", index))
            }
        }
        PersistenService.saveContext()
    }
    
    static func deleteReadings() -> Int{
        var count = 0
        let readings: [Reading] = fetchReadings()
        for reading in readings {
            //reading.sensor?.removeFromReadings(reading)
            PersistenService.context.delete(reading)
            count += 1
        }
        PersistenService.saveContext()
        return count
    }
    
    static func findFirstTimestamp(ascendingSort: Bool) -> Int64{
        let fetchRequest:  NSFetchRequest<Reading> = Reading.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Reading.timestamp), ascending: ascendingSort)
        fetchRequest.fetchLimit = 1;
        fetchRequest.sortDescriptors = [sort]
        var fetchedReadings: [Reading] = []
        do{
            fetchedReadings  = try PersistenService.context.fetch(fetchRequest)
        } catch {}
        return fetchedReadings[0].timestamp
    }
    
    static func avrageValueForAllReadings() -> Float {
        var avrage: Float = 0
        let keypathExpl = NSExpression(forKeyPath: "value")
        let expression = NSExpression(forFunction: "average:", arguments: [keypathExpl])
        let avrageDesc = NSExpressionDescription()
        avrageDesc.expression = expression
        avrageDesc.name = "avrageValues"
        avrageDesc.expressionResultType = .floatAttributeType
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Reading") //<Reading> = Reading.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.propertiesToFetch = [avrageDesc]
        fetchRequest.resultType = .dictionaryResultType
        
        var fetchedValues: [[String:Float]] = []
        do{
            fetchedValues  = try PersistenService.context.fetch(fetchRequest) as! [[String:Float]]
        } catch {}
        if let fetchedFloatReadings = fetchedValues.first?["avrageValues"] {
            avrage = fetchedFloatReadings
        }

        return avrage
    }
    
    static func avrageValueForEachSensor() -> [(key:String,value:Float)] {
        let sensors = SensorManager.fetchSensors()
        var avrages: [String:Float] = [:]
        
        let keypathExpl = NSExpression(forKeyPath: "value")
        let expression = NSExpression(forFunction: "average:", arguments: [keypathExpl])
        let avrageDesc = NSExpressionDescription()
        avrageDesc.expression = expression
        avrageDesc.name = "avrageValues"
        avrageDesc.expressionResultType = .floatAttributeType

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Reading") //<Reading> = Reading.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.propertiesToFetch = [avrageDesc]
        fetchRequest.resultType = .dictionaryResultType
        
        

        var fetchedReadings: [[String:Float]] = []
        
        for element in sensors {
            
            fetchRequest.predicate = NSPredicate(format: "sensor.name == %@", element.name!)
            do{
                fetchedReadings  = try PersistenService.context.fetch(fetchRequest) as! [[String:Float]]
            } catch {}

            if let fetchedFloatReadings = fetchedReadings.first?["avrageValues"] {
                avrages.updateValue(fetchedFloatReadings, forKey: element.name!)
            }
           
        }
        let sortedAvrages = avrages.sorted(by:{first,second in return  first.key < second.key; } )
        return sortedAvrages
    }
    
    static func randomDateFromYearBefore() -> Int64{
        let backInTime = Int64(arc4random_uniform(31556926))
        let currentTime = Int64(NSDate().timeIntervalSince1970)
        return currentTime - backInTime
    }
    
    static func randomInt(max: Int) -> Int{
        return Int(arc4random_uniform(UInt32(max)))
    }
    
    static func randomFloat() -> Float{
        return Float(arc4random()) / Float(UInt32.max) * 100
    }
}
