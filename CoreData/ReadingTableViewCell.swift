//
//  ReadingTableViewCell.swift
//  CoreData
//
//  Created by Pawel Srokowski on 05/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import UIKit

class ReadingTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timestampLabel:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
