//
//  SensorManager.swift
//  Lab1CoreData
//
//  Created by Pawel Srokowski on 15/01/2018.
//  Copyright © 2018 Pawel Srokowski. All rights reserved.
//

import Foundation
import CoreData
class SensorManager{
    static func createSensors(number: Int){
        for i in 0 ... number {
            let newSensor = NSEntityDescription.insertNewObject(forEntityName: "Sensor", into: PersistenService.context) as! Sensor
            newSensor.name = String(format: "S%02d",i)
            newSensor.desc = String(format: "Sensor number S%02d",i)
        }
        PersistenService.saveContext()
    }
    
    static func fetchSensors() -> [Sensor] {
        let fetchRequest:  NSFetchRequest<Sensor> = Sensor.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Sensor.name), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        var fetchedSensors: [Sensor] = []
        do{
            fetchedSensors = try PersistenService.context.fetch(fetchRequest)
        } catch {}
        return fetchedSensors
    }
}
