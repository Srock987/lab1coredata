//
//  RecordingTableViewController.swift
//  CoreData
//
//  Created by Pawel Srokowski on 15/12/2017.
//  Copyright © 2017 Pawel Srokowski. All rights reserved.
//

import UIKit
import CoreData

class ReadingTableViewController: UITableViewController {

    // MARK: - Table view data source
    var readings: [Reading] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    func loadData(){
        self.readings = ReadingManager.fetchReadings();
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return readings.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reading = readings[indexPath.row]
        let sensor = reading.sensor
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadingTableViewCell", for: indexPath) as! ReadingTableViewCell
        cell.valueLabel?.text = String(describing: reading.value)
        cell.timestampLabel?.text = String(describing: reading.timestamp)
        cell.nameLabel?.text = sensor?.name
        return cell
    }
 

}
